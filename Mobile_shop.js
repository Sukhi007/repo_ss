const express = require('express');
const path = require('path');
const bodyparser = require('body-parser');
const {check,validationResult} = require('express-validator');
const myApp = express();

myApp.use(bodyparser.urlencoded({
    extended:false}));

myApp.use(bodyparser.json());

myApp.set('views',path.join(__dirname,'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine','ejs');

myApp.get('/',function(req,res){
    res.render('Mobile_shop');
});
myApp.post('/Mobile_shop',[
    check('name','Please enter the name').not().isEmpty(),
    check('Email','Please enter a valid email').isEmail(),
    check('phone','Please enter a valid phoneNumber').isMobilePhone(),
    check('qty','Please enter a quantity').isInt(),
    check('Confirmphone').custom((value,{req}) =>{
        if(value !== req.body.phone){
            throw new Error('Number do not match');
        }
        return true;
    }),
    check('qty','Please enter a positive value').custom(value =>{
        value = parseInt(value)
        if(value < 0){
            throw new Error('Negative value found');
        }
        return true;
    }),
    ],function(req,res){
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        var errorData = {
            errors: errors.array()
        }
        res.render('_layouts/Mobile_shop',errorData);
    }
    else
    {
    const name = req.body.name;
    const phone = req.body.phone;
    const qty = req.body.qty;
    const pageData = {
        name: name,
        phone: phone,
        qty: qty
    };
    res.render('_layouts/Mobile_shop',pageData);
    }
});

myApp.listen(8080);
console.log('Server started at 8080 for mywebsite...');