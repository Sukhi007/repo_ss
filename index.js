var express = require('express');
var path = require('path');
var bodyparser = require('body-parser');
var myApp = express();

myApp.use(bodyparser.urlencoded({
    extended:false}));

myApp.use(bodyparser.json());

myApp.set('views',path.join(__dirname,'views'));
myApp.use(express.static(__dirname+'/public'));
myApp.set('view engine','ejs');

myApp.get('/',function(req,res){
    res.render('index');
});
myApp.get('/halloween',function(req,res){
    res.render('halloween');
});
myApp.get('/hello',function(req,res){
    res.render('hello');
});
myApp.get('/contact',function(req,res){
    res.render('_layouts/Contactform');
});
myApp.post('/contact',function(req,res){

    var name = req.body.name;
    var phone = req.body.phone;
    var qty = req.body.qty;
    var pageData = {
        name: name,
        phone: phone,
        qty: qty
    };
    res.render('_layouts/Contactthankyou',pageData);
    
});


myApp.listen(8080);
console.log('Server started at 8080 for mywebsite...');
