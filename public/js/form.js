 /*
    The function formSubmit() is called when the form "myform" is submitted.
    It should run some validations and show the output.
*/
var taxRate="";
function formSubmit(){
    var fullName=document.getElementById('fullname').value;
    var email=document.getElementById('email').value;
    var phoneNumber=document.getElementById('phone').value;
    var address=document.getElementById('address').value;
    var city=document.getElementById('city').value;
    var postcode=document.getElementById('postcode').value;
    var province=document.getElementById('province').value;
    var cover=document.getElementById('cover').value;
    var charger=document.getElementById('charger').value;
    var airpods=document.getElementById('airpods').value;
    var delivery_time=document.getElementById('deliverytime').value;
    var rePostCode=/^[A-Z][0-9][A-Z]\s[0-9][A-Z][0-9]$/;
    var rePhoneNumber=/^[0-9]{3}\-[0-9]{3}\-[0-9]{4}$/;
    var reEmail=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var reCharacter=/^[A-Za-z\s]{1,}$/;
    var reOrder=/^[0-9]$/;
    var errors="";
         
    if(fullName.trim()==='')
    {
        errors+='Name is required.<br>';
    }           
    else if(!reCharacter.test(fullName))
    { 
        errors+='Name should contain alphabets only.<br>';
    }

    if(email.trim()==='')
    {
        errors+='Email is required.<br>';
    }
    else if(!reEmail.test(email))
    {
        errors+='Email is not in correct form.<br>';
    }

    if(phoneNumber.trim()==='')
    {
        errors+='Phone Number is required.<br>';
    }
    else if(!rePhoneNumber.test(phoneNumber))
    {
        errors+='Phone number should contain numbers like 123-123-1234.<br>';
    }

    if(address.trim()==='')
    {
        errors+='Address is required.<br>';
    }

    if(city.trim()==='')
    {
        errors+='City is required.<br>';
    }
    else if(!reCharacter.test(city))
    {
        errors+='City field should contain alphabets only.<br>';
    }

    if(postcode.trim()==='')
    {
        errors+='Postal code is required.<br>';
    }
    else if(!rePostCode.test(postcode))
    {
        errors+='Postcode should be like X9X 9X9.<br>';
    }

    if(cover=='')
    {
        cover=0;
    }
    else if(cover<=0)
    {
        errors+='You cannot order 0 phone Cover.<br>';
    }
    else if(!reOrder.test(cover))
    {
        errors+='Quantity for cover should be a number.<br>';
    }

    if(charger=='')
    {
        charger=0;     
    }
    else if(charger<=0)
    {
        errors+='You cannot order 0 phone charger.<br>';
    }
    else if(!reOrder.test(charger))
    {
        errors+='Quantity for charger should be a number.<br>';
    }

    if(airpods=='')
    {
        airpods=0;     
    }
    else if(airpods<=0)
    {
        errors+='You cannot order 0 airpods.<br>';
    }
    else if(!reOrder.test(airpods))
    {
        errors+='Quantity for airpods should be a number.<br>';
    } 

    if(charger=='' && cover=='' && airpods=='')
    {
        errors+='You have to buy atleast one product.<br>';
    }

    if(delivery_time==="")
    {
        errors+='Please select delivery time.<br>';
    }

    if(province==="")
    {
        errors+='Please select province.<br>';
    }

    if(errors)
    {
        document.getElementById('errors').style.display = 'block';
        document.getElementById('errors').style.border = '2px';
        document.getElementById('errors').innerHTML=errors;
    } 
    else
    {
        $('.formData').show();
        $('form').hide(); 
        TaxRate(province);
        console.log("province ",province+" "+delivery_time+" "+taxRate);
        document.getElementById('errors').innerHTML='';
        var sub_total=parseInt(cover)*100+parseInt(charger)*200+parseInt(airpods)*250+parseInt(delivery_time);
        var total_tax=parseInt(sub_total)*parseFloat(taxRate);
        var total_charges=sub_total+total_tax;
       
        var result=`<span id="span_left">Name </span> <p id="span_center">: <span id="span_right">  ${fullName}</span></p>
                    <span id="span_left">Email </span>  <p id="span_center">:<span id="span_right"> ${phoneNumber}</span></p>
                    <span id="span_left">Delivery Address </span> <p id="span_center">:<span id="span_right"> ${address}</span></p>
                    <span id="span_right"> ${city}&nbsp;,${province},&nbsp;&nbsp;${postcode}</span><br>
                    <span id="span_left"><br> ${cover} Cover @ 100</span><p id="span_center">:<span id="span_right">$${cover*100}</span></p>
                    <span id="span_left">${charger} Charger @200 </span><p id="span_center">:<span id="span_right"> $${charger*200}</span></p>
                    <span id="span_left">${airpods} Airpods @ 250</span><p id="span_center">: <span id="span_right"> $${airpods*250}</span></p>
                    <span id="span_left">Shipping Charges</span><p id="span_center">: <span id="span_right">  $${delivery_time}</span></p>
                    <span id="span_left">Sub Total</span><p id="span_center">:  <span id="span_right">$${sub_total}</span></p>
                    <span id="span_left">Taxes @ ${(taxRate*100).toFixed(1)}%</span> <p id="span_center">: <span id="span_right">$${total_tax.toFixed(2)}</span></p>
                    <span id="span_left">Total</span> <p id="span_center">: <span id="span_right">$${total_charges}</span></p>`; 

        document.getElementById('formResult').innerHTML=result;
    }

    // Return false will stop the form from submitting and keep it on the current page.
    return false;
}
 
function TaxRate(province)
{
    switch(province)
    {   
        case 'AB':taxRate=0.05;
            break;
        case "BC":taxRate=0.12;
            break;
        case "MB":taxRate=0.13;
            break;
        case "NB":taxRate=0.15;
            break;
        case "NL":taxRate=0.15;
            break;
        case "NS":taxRate=0.15;
            break;
        case "NT":taxRate=0.05;
            break;
        case "NU":taxRate=0.05;
            break;
        case "ON":taxRate=0.13;
            break;
        case "PE":taxRate=0.15;
            break;
        case "QC":taxRate=0.149;
            break;
        case "SK":taxRate=0.11;
            break;
        case "YT":taxRate=0.05;
            break;
    }
}
